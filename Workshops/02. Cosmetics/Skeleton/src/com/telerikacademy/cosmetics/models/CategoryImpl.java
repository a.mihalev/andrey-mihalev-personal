package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    //use constants for validations values
    
    private String name;
    private List<Product> products;
    public CategoryImpl(String name) {
        this.name = name;
        this.products = new ArrayList<>();
        //validate
        //initialize the collection
    }
    
    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //We are making shadow copy of the array and this copy is given to the user for manipulating.
        // We can see what was in the array before the outside changes of the collection. Also, if we are adding things
        // to the array, we are adding the only in the copy, without changing the other array,
        // but if we manipulate whit the objects in the new array we will change them in also in the first one.
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        products.add(product);


    }
    
    public void removeProduct(Product product) {
        products.remove(product);
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        StringBuilder builder = new StringBuilder();
        builder.append("#Category: " + name + "\n");
        if (products.size()==0){
            builder.append(" #No products in this category!");
        }else {
            for (Product product:products) {
                builder.append(product.print());

            }
        }
        return builder.toString();
        //return String.format()
        //finish ProductBase class before implementing this method


    } }

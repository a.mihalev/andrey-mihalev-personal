package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {
    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);

    }

    public List<String> getIngredients() {
        return ingredients;
    }


    private void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException("Ingredients can't be null!");
        }
        this.ingredients = ingredients;
    }

    @Override
    public GenderType getGender() {
        return super.getGender();
    }

    @Override
    public String print() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ingredients.size(); i++) {

            if (i == ingredients.size()-1){
                builder.append(this.ingredients.get(i));
            }else {
                builder.append(this.ingredients.get(i) + ", " );
            }
        }

        return String.format("#%s %s%n" +
                "  #Price: $%.2f%n" +
                "  #Gender: %s%n" +
                "  #Ingredients: [ %s ]%n" +
                "  ===", getName(), getBrand(), getPrice(), getGender(), builder);
    }
}

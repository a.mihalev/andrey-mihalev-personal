package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductBase implements Cream{
    private ScentType scent;


    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name,brand,price,gender);
        this.scent = scent;


    }


    @Override
    public ScentType getScent() {
        return this.scent;
    }

    @Override
    public GenderType getGender() {
        return super.getGender();
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                "  #Price: $%.2f%n" +
                "  #Gender: %s%n" +
                "  # Scent: %s%n" +
                "  ===",getName(), getBrand(), getPrice(), getGender(), getScent());
    }
}



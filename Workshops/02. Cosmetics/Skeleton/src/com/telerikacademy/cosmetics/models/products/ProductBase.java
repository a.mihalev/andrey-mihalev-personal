package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;

public class ProductBase {
    //Finish the class
    //implement proper interface (see contracts package)
    //validate
    public static final int MIN_NAME_LENGHT = 3;
    public static final int MAX_NAME_LENGHT = 10;
    public static final int MIN_BRAND_NAME_LENGHT = 2;
    public static final int MAX_BRAND_NAME_LENGHT = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;

    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null){
            throw new IllegalArgumentException("Name can't be null");
        }

            if(name.length() < MIN_NAME_LENGHT || name.length() > MAX_NAME_LENGHT) {
            throw new IllegalArgumentException(String.format("Name must be minimum %d symbols and " +
                    "maximum %d symbols long!", MIN_NAME_LENGHT, MAX_NAME_LENGHT));
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if(brand==null){
            throw new IllegalArgumentException("Brand can't be null");
        }
        if (brand.length() < MIN_BRAND_NAME_LENGHT || brand.length() > MAX_BRAND_NAME_LENGHT) {
            throw new IllegalArgumentException(String.format("Brand must be minimum %d symbols and " +
                    "maximum %d symbols long!", MIN_BRAND_NAME_LENGHT, MAX_BRAND_NAME_LENGHT));
        }
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative!");
        }
        this.price = price;

    }
    public GenderType getGender(){
        return this.gender;
    }
}
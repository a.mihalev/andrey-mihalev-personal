package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductBase implements Shampoo {
    private int milliliters;
    private UsageType everyDay;
    
    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType everyDay) {
        super(name, brand, price, gender);
        this.everyDay = everyDay;
        this.milliliters = milliliters;

    }

    public int getMilliliters() {
        return this.milliliters;
    }

    @Override
    public UsageType getUsage() {
        return this.everyDay;
    }

    private void setMilliliters(int milliliters) {
        if (milliliters<0){
            throw new IllegalArgumentException("Milliliters can't be negative number ");
        }
        this.milliliters = milliliters;
    }

    @Override
    public GenderType getGender() {
        return super.getGender();
    }

    @Override
    public String print() {
         return String.format("#%s %s%n" +
                         "  #Price: $%.2f%n" +
                         "  #Gender: %s%n" +
                         "  #Milliliters: %d%n" +
                 "  #Usage: %s%n" +
                 "  ===", getName(), getBrand(), getPrice(), getGender(), getMilliliters(), getUsage());
    }
}

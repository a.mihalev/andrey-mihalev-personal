package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private double price;
    private String name;
    private String brand;
    private final GenderType gender;
    
    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;

        // finish the constructor and validate data
    //    setPrice(price);
    }

    public String getName() {
        return name;
    }

    private void setName(String name){
        if ((name.length()<3) || (name.length()>10)){
            throw new IllegalArgumentException("Minimum product name’s length is 3 symbols and maximum is 10 symbols!");
        }else {
            this.name=name;
        }
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand){
        if((brand.length()<2)||(brand.length()>10)){
            throw new IllegalArgumentException("Minimum brand name’s length is 2 symbols and maximum is 10 symbols.");
        }else {
            this.brand = brand;
        }
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(Double price){
        if (price<0){
            throw new IllegalArgumentException("Price cannot be negative");
        }else {
            this.price = price;
        }
    }
    
    public String print() {
        return String.format("#[%s] [%s]%n#Price: [%.2f]%n#Gender: [%s]%n=====", name,brand,price, String.valueOf(gender));

        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }
    
}

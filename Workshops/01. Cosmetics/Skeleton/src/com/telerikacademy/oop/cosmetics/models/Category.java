package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {
    
    private String name;
    private List<Product> products;
    static int count = 0;
    
    public Category(String name) {
        if (name.length()<2||name.length()>15){
            throw new IllegalArgumentException("Minimum category name’s length length is 2 symbols and maximum is 15 symbols.");

        }
        this.name = name;
        this.products = new ArrayList<Product>();

    }
    
    public List<Product> getProducts() {
        return this.products;
    }
    
    public void addProduct(Product product) {
        if (product==null){
            throw new IllegalArgumentException("Product can't be null");
        }
        products.add(product);
        count++;
    }
    
    public void removeProduct(Product product) {
        if (product==null){
            throw new IllegalArgumentException("Product can't be null");
        }
        if(products.contains(product)){
            products.remove(product);
            count--;
        }else {
            throw new IllegalArgumentException("No such product!");
        }
    }
    
    public String print() {
        StringBuilder builder = new StringBuilder();
        builder.append("#Category: " + name + "\n");
        if (products.size()==0){
            builder.append("No product in this category");
        }else {
            for (Product product:products) {
                builder.append(product.print());

            }
        }
        return builder.toString();

    }


    }

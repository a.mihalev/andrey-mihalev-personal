package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList;
    
    public ShoppingCart() {
        productList= new ArrayList<Product>();

    }
    
    public List<Product> getProductList() {

        return productList;
    }
    
    public void addProduct(Product product) {
        if(product==null){
            throw new IllegalArgumentException("Invalid product");

        }
        productList.add(product);

    }
    
    public void removeProduct(Product product) {
        if(product==null){
            throw new IllegalArgumentException("Product is null!");
        }
        productList.remove(product);

    }
    
    public boolean containsProduct(Product product) {
        if(product==null){
            throw new IllegalArgumentException("Product is null!");
        }
        if(productList.contains(product)){
            return true;
        }else {
            return false;
        }
    }
    
    public double totalPrice() {
        double totalPrice=0;
        for (Product prod : productList) {
            totalPrice+=prod.getPrice();

        }
        return totalPrice;

    }



    }
    


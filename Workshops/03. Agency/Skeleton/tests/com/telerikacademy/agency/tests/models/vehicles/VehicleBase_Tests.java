package com.telerikacademy.agency.tests.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.VehicleBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VehicleBase_Tests {
    
    private class VehicleBaseTestClass extends VehicleBase {
        
        public VehicleBaseTestClass(int passengerCapacity, double pricePerKilometer, VehicleType type) {
            super(passengerCapacity, pricePerKilometer, type);
        }
        
        @Override
        public String printClassName() {
            return null;
        }
        
    }
    
    @Test
    public void constructor_should_throw_when_passengerCapacityLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new VehicleBaseTestClass(0, 1, VehicleType.LAND));
        
    }
    
    @Test
    public void constructor_should_throw_when_passengerCapacityMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new VehicleBaseTestClass(801, 1, VehicleType.LAND));
        
    }
    
    @Test
    public void constructor_should_throw_when_pricePerKmLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new VehicleBaseTestClass(100, 0, VehicleType.LAND));
        
    }
    
    @Test
    public void constructor_should_throw_when_pricePerKmMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new VehicleBaseTestClass(100, 3, VehicleType.LAND));
        
    }
    
    @Test
    public void constructor_should_throw_when_typeIsNull() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new VehicleBaseTestClass(100, 2, null));
        
    }
    
}

package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {
    private static final int MIN_PASSENGERS = 10;
    private static final int MAX_PASSENGERS = 50;
    private static final String BUS_PASSENGERS_EXCEPTION = "A bus cannot have less than 10 passengers or more than 50 passengers.";

    VehicleType type;


    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);
        if(passengerCapacity < MIN_PASSENGERS || passengerCapacity> MAX_PASSENGERS){
            throw new IllegalArgumentException(BUS_PASSENGERS_EXCEPTION);
        }
        this.passengerCapacity = passengerCapacity;
        this.type = VehicleType.LAND;

    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String print() {
        System.out.println("Bus----");
        return  super.print();

    }
}

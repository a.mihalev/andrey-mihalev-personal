package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {
    private static final int MAX_PASSENGERS = 150;
    private static final int MIN_PASSENGERS = 30;
    private static final String MAX_MIN_PASSENGERS_EXCEPTION = "A train cannot have less than 30 " +
                                                                "passengers or more than 150 passengers.";
    private static final int MIN_CARTS_PER_TRAIN = 1;
    private static final int MAX_CARTS_PER_TRAIN = 15;
    private static final String NUMBER_OF_CARTS_EXCEPTION = "A train cannot have less than 1 cart or more than 15 carts.";

    private Integer carts;
    private VehicleType type;



    public TrainImpl(int passengerCapacity, double pricePerKilometer, Integer carts) {
        super(passengerCapacity, pricePerKilometer);
        if(passengerCapacity<MIN_PASSENGERS || passengerCapacity>MAX_PASSENGERS) {
            throw new IllegalArgumentException(MAX_MIN_PASSENGERS_EXCEPTION);
        }
        this.passengerCapacity = passengerCapacity;
        setCarts(carts);
        this.type = VehicleType.LAND;

      }

    private void setCarts(Integer carts) {
        if(carts==null){
            throw new IllegalArgumentException("Carts can't be null");
        }
        if(carts<MIN_CARTS_PER_TRAIN || carts>MAX_CARTS_PER_TRAIN){
            throw new IllegalArgumentException(NUMBER_OF_CARTS_EXCEPTION);
        }
        this.carts = carts;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public int getCarts() {
        return this.carts;
    }

    @Override
    public String print(){
        return String.format("Train ----%n" + super.print()+
                "Carts amount: %d%n", getCarts());
    }
}

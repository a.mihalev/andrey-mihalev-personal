package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {
    private Journey journey;
    private Double administrativeCosts;

    
    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return this.journey;
    }

    @Override
    public double calculatePrice() {

        return journey.calculateTravelCosts()*administrativeCosts;
    }
    @Override
    public String print() {
        return String.format("Ticket -----%n" +
                "Destination: %s%n" +
                "Price: %.2f%n", journey.getDestination(), calculatePrice());
    }

    @Override
    public String toString() {
        return print();
    }
}

package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public  class VehicleBase implements Vehicle {
    private static final String TYPE_CANT_BE_NULL = "Type can't be null!";
    private static final String PASSENGERS_CAPACITY_CANT_BE_NULL = "Passengers can't be null!";
    static final String PRICE_CANT_BE_NULL = "Price can't be null!";
    private static final double MINIMAL_PRICE_PER_KILOMETER = 0.1;
    private static final double MAXIMAL_PRICE_PER_KILOMETER = 2.5;
    private static final String ERR_PRICE_PER_KILOMETER = "A vehicle with a price per kilometer lower " +
                                                            "than $0.10 or higher than $2.50 cannot exist!";
    private static final int MIN_PASSENGERS_FOR_VEHICLE = 1;
    private static final int MAX_PASSENGERS_FOR_VEHICLE = 800;
    private static final String ERR_PASSENGERS_COUNT = "A vehicle with less than 1 passengers or " +
                                                        "more than 800 passengers cannot exist!";



    Integer passengerCapacity;
    Double pricePerKilometer;
    VehicleType type;

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }
    public VehicleBase(int passengerCapacity, double pricePerKilometer){
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }






    private void setPassengerCapacity(Integer passengerCapacity) {
        if (passengerCapacity == null) {
            throw new IllegalArgumentException(PASSENGERS_CAPACITY_CANT_BE_NULL);
        }
        if(passengerCapacity<MIN_PASSENGERS_FOR_VEHICLE||passengerCapacity>MAX_PASSENGERS_FOR_VEHICLE){
            throw new IllegalArgumentException(ERR_PASSENGERS_COUNT);
        }
        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(Double pricePerKilometer) {
        if (pricePerKilometer == null) {
            throw new IllegalArgumentException(PRICE_CANT_BE_NULL);
        }
        if(pricePerKilometer<MINIMAL_PRICE_PER_KILOMETER||pricePerKilometer>MAXIMAL_PRICE_PER_KILOMETER){
            throw new IllegalArgumentException(ERR_PRICE_PER_KILOMETER);
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    private void setType(VehicleType type) {
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
        this.type = type;
    }

    public String printClassName(){
        return String.format("%s", getClass().toString());
    }
    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }




    @Override
    public String print() {
        return String.format("Passenger capacity: %d%nPrice per kilometer: %.2f%nVehicle type: %s%n",
                getPassengerCapacity(), getPricePerKilometer(), getType());

}

    @Override
    public String toString() {
        return print();
    }
}
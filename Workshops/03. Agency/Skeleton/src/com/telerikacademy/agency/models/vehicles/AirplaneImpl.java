package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {
    private Boolean hasFood;
    private VehicleType type;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, Boolean hasFood) {
        super(passengerCapacity, pricePerKilometer);
        this.type = VehicleType.AIR;
        setHasFood(hasFood);

    }

    public Boolean getHasFood() {
        return this.hasFood;
    }

    private void setHasFood(Boolean hasFood) {
        if(hasFood==null){
            throw new IllegalArgumentException("Food can't be null!");
        }
        this.hasFood = hasFood;
    }

    @Override
    public boolean hasFreeFood() {
        return this.hasFood;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    public String print() {
        return String.format("Airplane ----%n" + super.print() + "Has free food: %s%n", getHasFood());

    }
    }


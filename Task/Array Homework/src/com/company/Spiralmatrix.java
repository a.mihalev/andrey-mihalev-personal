package com.company;

import java.util.Scanner;

public class Spiralmatrix {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int input = sc.nextInt();

        int[][] spiralMatrix = new int[input][input];

        int value = 1;

        int firstCol = 0;
        int lastCol = input - 1;
        int firstRow = 0;
        int lastRow = input - 1;

        while (value <= input * input) {
            for (int i = firstCol; i <= lastCol; i++) {
                spiralMatrix[firstRow][i] = value;

                value++;
            }

            for (int i = firstRow + 1; i <= lastRow; i++) {
                spiralMatrix[i][lastCol] = value;

                value++;
            }

            for (int i = lastCol - 1; i >= firstCol; i--) {
                spiralMatrix[lastRow][i] = value;

                value++;
            }

            for (int i = lastRow - 1; i >= firstRow + 1; i--) {
                spiralMatrix[i][firstCol] = value;

                value++;
            }

            firstCol++;

            firstRow++;

            lastCol--;

            lastRow--;
        }

        for (int i = 0; i < spiralMatrix.length; i++) {
            for (int j = 0; j < spiralMatrix.length; j++) {
                System.out.print(spiralMatrix[i][j] + "\t");
            }

            System.out.println();
        }
    }
}

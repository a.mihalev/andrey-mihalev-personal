package com.company;

import java.util.Scanner;

public class Bounce {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int input = Integer.parseInt(sc.nextLine());
        Double[][] matrix = new Double[input][input];

        for (int row = 0; row < input; row++) {
            double firstNum = Math.pow(2, row);
            for (int col = 0; col < input; col++) {

                if (col == 0) {
                    matrix[row][col] = firstNum;
                } else {
                    matrix[row][col] = firstNum * (Math.pow(2, col));
                }


            }

        }
    }
}

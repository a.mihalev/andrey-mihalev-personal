package com.company;

import java.util.Scanner;

public class aboveDiagonal {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int input = Integer.parseInt(sc.nextLine());
        Double[][] matrix = new Double[input][input];

        for (int row = 0; row < input; row++) {
            double firstNum = Math.pow(2, row);
            for (int col = 0; col < input; col++) {

                if (col == 0) {
                    matrix[row][col] = firstNum;
                } else {
                    matrix[row][col] = firstNum * (Math.pow(2, col));
                }


            }

        }
//        for (int i = 0; i < input; i++) {
//            for (int j = 0; j < input; j++) {
//                System.out.printf("%.0f ", matrix[i][j]);
//
//            }
//            System.out.println();

//        }
        double sum = 0;
        for (int j = 0; j < input; j++) {
            for (int i = j; i >= 0; i--) {
                sum = sum + matrix[i][j];
            }

        }
        System.out.printf("%.0f ", sum);

    }

}



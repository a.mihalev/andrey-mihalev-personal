import org.asynchttpclient.util.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BingTests {
    @Test
    public void navigateToBing(){
        System.setProperty("webdriver.chrome.driver", "D:\\telerik\\Andrey mihalev Repo\\andrey-mihalev-personal\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        //Arrange
        webDriver.get("http://bing.com");
        // ACT
        WebElement searchInput =  webDriver.findElement(By.id("sb_form_q"));
        //Assert
        Assert.assertTrue("Search input is not displayed!",searchInput.isDisplayed());
        webDriver.close();


    }

    @Test
    public void navigateToBing_byName(){
        System.setProperty("webdriver.chrome.driver", "D:\\telerik\\Andrey mihalev Repo\\andrey-mihalev-personal\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        //Arrange
        webDriver.get("http://bing.com");
        // ACT
        WebElement searchInput =  webDriver.findElement(By.id("q"));
        //Assert
        Assert.assertTrue("Search input is not displayed!",searchInput.isDisplayed());
        webDriver.close();


    }
    @Test
    public void navigateToBing_byClass(){
        System.setProperty("webdriver.chrome.driver", "D:\\telerik\\Andrey mihalev Repo\\andrey-mihalev-personal\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        //Arrange
        webDriver.get("http://bing.com");
        // ACT
        WebElement searchInput =  webDriver.findElement(By.className("sb_form_q"));
        //Assert
        Assert.assertTrue("Search input is not displayed!",searchInput.isDisplayed());
        webDriver.close();


    }
    @Test
    public void navigateToBing_byExpath(){
        System.setProperty("webdriver.chrome.driver", "D:\\telerik\\Andrey mihalev Repo\\andrey-mihalev-personal\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        //Arrange
        webDriver.get("http://bing.com");
        // ACT
        WebElement searchInput =  webDriver.findElement(By.xpath("//input[@id = 'sb_form_q']"));
        //Assert
        Assert.assertTrue("Search input is not displayed!",searchInput.isDisplayed());
        webDriver.close();


    }
    @Test
    public void navigateToBing_Link(){
        System.setProperty("webdriver.chrome.driver", "D:\\telerik\\Andrey mihalev Repo\\andrey-mihalev-personal\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();


        //Arrange
        webDriver.get("http://bing.com");
        // ACT
        WebElement searchInput =  webDriver.findElement(By.linkText("Images"));
        //Assert
        Assert.assertTrue("Search input is not displayed!",searchInput.isDisplayed());
        webDriver.close();


    }


}

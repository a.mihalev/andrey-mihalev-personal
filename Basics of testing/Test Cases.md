 #TEST CASES
 
#Test case 1

###Title
- Order a taxi as a registered user of the app

### Description
- As a registered user of the app, order a taxi to pick you up from your location
and drives you to current destination.

###Prerequisites
- Registered user for the taxi ordering app.
- your current location
- arrival destination

###Steps
1. Navigate to the app and open it.
2. Fill your pick-up location in the initial point field.
3. Fill your arrival destination in the end point field.
4. Send your request via send button.


# Test case 2

###Title
- Order a taxi as a registered user of the app and choose payment method - cash.

### Description
- As a registered user of the app, order a taxi to pick you up from your location
  and drives you to current destination and choose to pay in cash 
  directly to the driver

###Prerequisites
- Registered user for the taxi ordering app.
- your current location
- arrival destination

###Steps
1. Navigate to the app and open it.
2. Fill your pick-up location in the initial point field.
3. Fill your arrival destination in the end point field.
4. From the payment methods button, choose "Cash".   
5. Send your request via send button.


# Test case 3

###Title

- Check where your taxi is, after placing an order as registered user of the app

### Description
-  As a registered user of the app, after  placing an order for a taxi to pick you up from your location and 
  drives you to current destination, check where your taxi is on his way, 
 

###Prerequisites
- Registered user for the taxi ordering app.
- your current location
- arrival destination

###Steps
1. Navigate to the app and open it.
2. Fill your pick-up location in the initial point field.
3. Fill your arrival destination in the end point field.
4. Send your request via the sendbutton.
5. Check where the taxi is on the map.


# Test case 4

###Title
- View the client requests as taxi driver

### Description
- As taxi driver try to see client requests in the app.  

###Prerequisites
- Sent request for taxi from an user.

###Steps
1. Navigate to the app and open it.
2. Go to the "New requests" button.
3. See the new requests.


#Test case 5

###Title
- Cancel client request as taxi driver

### Description
- As taxi driver try to cancel client requests.

###Prerequisites
- Sent request for taxi from another user.

###Steps
1. Navigate to the app and open it.
2. Go to the "New requests" button.
3. See the new requests.
4. Open one request.
5. Cancel it.


# Test case 6

###Title
- View the client requests

### Description
- As taxi driver try to see client requests.

###Prerequisites
- Sent request for taxi from an user.

###Steps
1. Navigate to the app and open it.
2. Go to the "New requests" button.
3. See the new requests.

# Test case 6

###Title
- Accept a client request as a taxi driver

### Description
- As taxi driver try accept a client requests.

###Prerequisites
- Sent request for taxi from an user.

###Steps
1. Navigate to the app and open it.
2. Go to the "New requests" button.
3. See the new requests.
4. Open one request.
5. Accept it.



import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;

public class BingTests {

    private WebDriver webDriver;

    @BeforeClass
    public static void driverSetup (){
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    public void setUp(){
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        webDriver = new ChromeDriver(options);

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(true);

        webDriver = new FirefoxDriver(firefoxOptions);

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.get("https://bing.com");


    }

    @After
    public void TearDown(){
        webDriver.quit();
    }

    @Test
    public void navigateToBing(){
        WebElement webElement = webDriver.findElement(By.id("sb_form_q"));
        Assert.assertTrue("Search input is not displayed", webElement.isDisplayed());

    }

    @Test
    public void









}

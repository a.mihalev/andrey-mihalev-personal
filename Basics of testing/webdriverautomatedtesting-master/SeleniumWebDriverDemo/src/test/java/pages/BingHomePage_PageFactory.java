package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BingHomePage_PageFactory {
    private WebDriver driver;

    public BingHomePage_PageFactory(WebDriver webDriver)
    {
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(name="q")
    public WebElement searchInput;

    @FindBy(xpath="//label[@for='sb_form_go']")
    public WebElement searchButton;

    public void searchTermAndSubmit(String term){
        searchInput.sendKeys(term);
        searchButton.click();
    }
}

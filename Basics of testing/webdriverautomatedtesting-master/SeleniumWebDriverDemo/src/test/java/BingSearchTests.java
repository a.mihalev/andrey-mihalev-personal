import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BingHomePage;
import pages.BingHomePage_PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.List;

public class BingSearchTests {

    private WebDriver webdriver;

    @BeforeClass
    public static void classSetup(){
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void setup(){
        // Chrome
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        webdriver = new ChromeDriver(options);

        // Firefox
        FirefoxBinary firefoxBinary = new FirefoxBinary();
//        firefoxBinary.addCommandLineOptions("--headless");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setBinary(firefoxBinary);
        webdriver = new FirefoxDriver(firefoxOptions);

        webdriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webdriver.get("https://bing.com");
    }

    @After
    public void tearDown(){
        webdriver.quit();
    }

    @Test
    public void navigateToBing(){
        // Act
        WebElement searchInput = webdriver.findElement(By.id("sb_form_q"));

        // Assert
        Assert.assertTrue("Search input was not displayed.", searchInput.isDisplayed());
    }

    @Test
    public void searchForTerm(){
        BingHomePage_PageFactory homePage = new BingHomePage_PageFactory(webdriver);

        homePage.searchTermAndSubmit("Telerik Academy Aplpha");

        WebElement resultsList = webdriver.findElement(By.id("b_results"));

        List<WebElement> results = resultsList.findElements(By.xpath(".//li"));

        // Assert
        Assert.assertTrue("Search input was not displayed.", results.get(0).isDisplayed());
    }

    @Test
    public void navigateToBing_waitElementByLinkText(){
        // Add Explicit Wait
        WebDriverWait wait = new WebDriverWait(webdriver, 10);

        // Act
        // Set Explicit wait Conditions
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Images")));

        WebElement imagesLink = webdriver.findElement(By.linkText("Images"));

        // Assert
        Assert.assertTrue("Images link was not displayed.", imagesLink.isDisplayed());
    }
}
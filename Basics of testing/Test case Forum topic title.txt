Test scenario: Testing creation of topic functionality.

Test case: User shouldn't be able to create topic whitout title.

Prerequisites

- existing account in https://stage-forum.telerikacademy.com/

Test data:
username: stanislav.nikolov.a28@learn.telerikacademy.com
password: Mechka

Test steps:
1. Launch Chrome browser in incognito mode.
2. Go to URL: https://stage-forum.telerikacademy.com/.
3. Log in with credentials.
4. Press "+ New topic" button.
5. Leave the title field empty. 
6. Fill the text field with sample text.
7. Press the "Create topic" button.

Expected result:
New topic should not be created, system should throw exception, that topic can't be created without title.

 
  
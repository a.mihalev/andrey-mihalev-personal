package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void hoverElement(String key, Object... arguments) {
        // TODO: Implement the method
        // 1. Get locator value from properties by key
        String locator = getLocatorValueByKey(key, arguments);
        // 2. Add Log entry for the action to be performed
        Utils.LOG.info("Hover on element " + key);
        // 3. Perform a hover Action
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(locator));
        action.moveToElement(element);

    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        // TODO: Implement the method
        // 1. Get iframe locator value from properties by key
        // 2. Add Log entry for the action to be performed
        // 3. Switch to the frame
    }

    //############# WAITS #########

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        // TODO: Implement the method
        String defaultTimeOut = "config.defaultTimeoutSeconds";
        waitForElementVisibleUntilTimeout(locatorKey,defaultTimeOut,arguments);

        // 1. Get default timeout from properties
        // 2. Use the method for waiting for element visible until timeout with the default timeout

    }

    public void waitForElementVisibleUntilTimeout(String locator, String seconds, Object... locatorArguments) {
        // TODO: Implement the method
        // 1. Initialize Wait utility with default timeout from properties
        String defaultTimeOut = "config.defaultTimeoutSeconds";
        WebDriverWait webDriverWait = new WebDriverWait(driver, Integer.parseInt(defaultTimeOut));

        String elementLocator = getLocatorValueByKey(locator,locatorArguments);
        // 2. Wait for Element Present
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementLocator)));

        // 3. Wait until Element is Visible
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));

        // 4. Fail the test with meaningful error message in case the element is not visible
        Assert.assertTrue("Element is not visible!", isElementVisible(elementLocator));


    }

    public void waitForElementPresent(String locator, Object... arguments) {
        // TODO: Implement the method
        // 1. Initialize Wait utility with default timeout from properties
        String defaultTimeOut = "config.defaultTimeoutSeconds";
        WebDriverWait webDriverWait = new WebDriverWait(driver, Integer.parseInt(defaultTimeOut));
        // 2. Use the method that checks for Element present
        String elementLocator = getLocatorValueByKey(locator, arguments);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementLocator)));
        // 3. Fail the test with meaningful error message in case the element is not present
        Assert.assertTrue("Element with locator " + elementLocator +
                " is not found on the page!",isElementPresent(elementLocator));
            }

    public boolean isElementPresent(String locator, Object... arguments) {
        // TODO: Implement the method
        // 1. Get default timeout from properties
        String defaultTimeOut = "config.defaultTimeoutSeconds";
        // 2. Initialize Wait untility
        WebDriverWait webDriverWait = new WebDriverWait(driver, Integer.parseInt(defaultTimeOut));
        // 3. Try to wait for element present
        String elementLocator = getLocatorValueByKey(locator, arguments);
        WebElement element = driver.findElement(By.xpath(elementLocator));
        // 4. return true/false if the element is/not present
        if(element.){
            return true;
        }else {
            return false;
        }



    }

    public boolean isElementVisible(String locator, Object... arguments) {
        // TODO: Implement the method
        // 1. Get default timeout from properties
        // 2. Initialize Wait untility
        // 3. Try to wait for element visible
        // 4. return true/false if the element is/not visible
        return true;
    }

    public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementAttribute(String locator, String attributeName, String attrubuteValue) {
        // TODO: Implement the method
        // 1. Find Element using the locator value from Properties
        // 2. Get the element attribute
        // 3. Assert equality with expected value
    }

    public void assertNavigatedUrl(String urlKey) {
        // TODO: Implement the method
        // 1. Get Current URL

        // 2. Get expected url by urlKey from Properties
    }

    public void pressKey(Keys key) {
        // TODO: Implement the method
        // 1. Initialize Actions
        // 2. Perform key press
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }
}

# TEST CREATING TECHNIQUES HOMEWORK

## Ideas for testing Telerik forum functionalities:
* Create a topic
* Post reply
* Change text formatting
* Notifications

### USED TECHNIQUES :
* Equivalence partitioning
* Boundary value anlysis
* Decision table
* Pairwise
* State transition
* Checked list

### Priority levels : 
- Prio 1 = HIGH  
- Prio 2 = MEDIUM 
- Prio 3 = LOW




| Test case title  | Priority  | Technique |
| :------------: |:---------------:| :-----:|
| New topic window should be open, when new topic button is pressed |Prio 1| State transition |
| Topic must be created only from registered users       | Prio 1       | Decision table |
| Welcome window should appear when user start filling the new topic fields |Prio 3| State transition|
| Administrator should be able to delete topics|Prio 1|Decision table; State transition|
| Topic title can't be empty   | Prio 1 | Boundary value analysis |
| Topic title lenght can't be more than X-number of symbols |Prio 1| Boundary value analysis |
| Topic title lenght cant be below X-number of symbols|Prio 1|Boundary value analysis |
| All users should be able to select multiple tags from the menu| Prio 3| Checked list|
| Usr should be notified for every reply in this topic when notification button is set to "Watching"|Prio 1|Decision table; Equivalence partitioning|
| User should be notified when his name is mentioned, and reply count should be shown, while notification button is set to "Tracking" |Prio 1| Decision table; Equivalence partitioning|
| User should be notified when his name is mentioned in reply, but reply count shouldn't be shown, when notification button is set to "Normal" |Prio 1|Decision table; Equivalence partitioning|
| User shouldn't be notified about anything in this topic| Prio 1 |Decison table; Equivalence partitioning|
| User should be able to post a reply to existing topic| Prio 1| State transition|
| Reply text lenght should be between X-minimal and Y-maximal number of symbols| Prio 2 | Boundary value analysis| 
| User should be able to change notifications level in existing topic| Prio 2| State transition|
| Default notification level should be "Watching" for every created post| Prio 3| Checked list|
| All formatting options should work simultaneously in single post or reply| Prio 1| Pairwise; Check list|
| User should be able to: paste image, insert image from device, paste hyperlink, insert emoji, bulleted or numbered list in the reply text field| Prio 3| Equivalence partitioning; Check list; Pairwise|

